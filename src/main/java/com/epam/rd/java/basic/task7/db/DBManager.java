package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.epam.rd.java.basic.task7.db.entity.*;

public class DBManager {
	private static DBManager instance = null;
	private static Connection connection;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
		String url = null;
		try (InputStream inputStream = new FileInputStream("app.properties")) {
			Properties properties = new Properties();
			properties.load(inputStream);
			url = properties.getProperty("connection.url");
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			connection = DriverManager.getConnection(url);

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		try (Statement statement = connection.createStatement();
			 ResultSet rs = statement.executeQuery(DBConstants.GET_ALL_USERS)) {
			while (rs.next()) {
				User user = new User();
				user.setId(rs.getInt("id"));
				user.setLogin(rs.getString("login"));
				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return users;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		try (Statement statement = connection.createStatement();
			 ResultSet rs = statement.executeQuery(DBConstants.GET_ALL_TEAMS)) {
			while (rs.next()) {
				Team team = new Team();
				team.setId(rs.getInt("id"));
				team.setName(rs.getString("name"));
				teams.add(team);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return teams;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		try (PreparedStatement statement = connection.prepareStatement(DBConstants.GET_USER)) {
			statement.setString(1, login);
			try (ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					user.setId(rs.getInt("id"));
					user.setLogin(rs.getString("login"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		try (PreparedStatement statement = connection.prepareStatement(DBConstants.GET_TEAM)) {
			statement.setString(1, name);
			try (ResultSet rs = statement.executeQuery()) {
				while (rs.next()) {
					team.setId(rs.getInt("id"));
					team.setName(rs.getString("name"));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return team;
	}

	public boolean insertUser(User user) throws DBException {
		try(PreparedStatement statement = connection.prepareStatement(DBConstants.INSERT_USER, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, user.getLogin());
			statement.executeUpdate();

			ResultSet rs = statement.getGeneratedKeys();
			int id=0;
			if (rs.next()) {
				id = rs.getInt(1);
				user.setId(id);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	public boolean insertTeam(Team team) throws DBException {
		try(PreparedStatement statement = connection.prepareStatement(DBConstants.INSERT_TEAM, Statement.RETURN_GENERATED_KEYS)) {
			statement.setString(1, team.getName());
			statement.executeUpdate();

			ResultSet rs = statement.getGeneratedKeys();
			int id=0;
			if (rs.next()) {
				id = rs.getInt(1);
				team.setId(id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement(DBConstants.SET_TEAM_FOR_USER)) {
			connection.setAutoCommit(false);
			for (Team team : teams) {
				statement.setInt(1, user.getId());
				statement.setInt(2, team.getId());
				statement.executeUpdate();
			}
			connection.commit();
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			e.printStackTrace();
			rollback(connection);
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	private void rollback(Connection connection) throws DBException {
		try {
			connection.rollback();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		ResultSet rs;

		try( PreparedStatement statement = connection.prepareStatement(DBConstants.GET_USER_TEAM)) {
			statement.setString(1,user.getLogin());
			rs = statement.executeQuery();

			while (rs.next()){
				teams.add(getTeam(rs.getString("name")));
			}

		} catch (SQLException e) {
			throw new DBException("Fail getUserTeams" + user.getLogin(),e);
		}

		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		try (PreparedStatement statement = connection.prepareStatement(DBConstants.DELETE_TEAM)) {
			statement.setInt(1, team.getId());
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		try(PreparedStatement statement = connection.prepareStatement(DBConstants.UPDATE_TEAM)) {
			statement.setString(1, team.getName());
			statement.setInt(2, team.getId());
			statement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}

	public boolean deleteUsers(User... users) throws DBException {
		try(PreparedStatement  statement = connection.prepareStatement(DBConstants.DELETE_USERS)) {
			for (User user : users) {
				statement.setInt(1, user.getId());
				statement.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new DBException(e.getMessage(), e);
		}
		return true;
	}
}
