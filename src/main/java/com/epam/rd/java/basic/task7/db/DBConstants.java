package com.epam.rd.java.basic.task7.db;

public abstract class DBConstants {
    private DBConstants() {
    }

    public static final String GET_ALL_USERS = "SELECT * FROM users";
    public static final String GET_ALL_TEAMS = "SELECT * FROM teams";
    public static final String GET_USER = "SELECT * FROM users WHERE login=?";
    public static final String GET_TEAM = "SELECT * FROM teams WHERE name=?";
    public static final String INSERT_USER = "INSERT INTO users (login) VALUES (?)";
    public static final String INSERT_TEAM = "INSERT INTO teams (name) VALUES (?)";
    public static final String SET_TEAM_FOR_USER = "INSERT INTO users_teams(user_id, team_id) VALUES (?, ?)";
    public static final String UPDATE_TEAM = "UPDATE teams SET name=? WHERE id=?";
    public static final String DELETE_USERS = "DELETE FROM users WHERE id=?";
    public static final String DELETE_TEAM = "DELETE FROM teams WHERE id=?";
    public static final String GET_USER_TEAM = "SELECT t.name, t.id FROM users_teams " +
            "INNER JOIN teams AS t ON users_teams.team_id = t.id " +
            "LEFT JOIN users AS u ON users_teams.user_id = u.id " +
            "WHERE u.login= ? ";
}
